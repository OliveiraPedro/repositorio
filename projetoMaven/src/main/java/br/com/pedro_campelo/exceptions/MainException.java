package br.com.pedro_campelo.exceptions;

public class MainException extends Exception {
	
	private static final String MSG="N�o foi poss�vel efetuar esta opera��o.Favor entrar em contato com o suporte.";
	
	public MainException(){
		super(MSG);
	}
	
	public MainException(Throwable e){
		super(MSG,e);
	}

	public MainException(String mensagem) {
		super(mensagem);
	}

}
