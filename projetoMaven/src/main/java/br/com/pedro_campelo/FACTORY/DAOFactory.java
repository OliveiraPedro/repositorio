package br.com.pedro_campelo.FACTORY;

import br.com.pedro_campelo.dao.ClienteDAO;
import br.com.pedro_campelo.jpa.ClienteJPADAO;


public class DAOFactory {
	
	@SuppressWarnings("rawtypes")
	public static Object getInstanceOf(Class c) {
		if ( c.equals(ClienteDAO.class) ) {
			return new ClienteJPADAO();
		}
		return null;
	}
}
