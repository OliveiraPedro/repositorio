package br.com.pedro_campelo.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.pedro_campelo.objetos.Cliente;
import br.com.pedro_campelo.service.ClienteService;

@Path("clienteREST")
public class ClienteREST extends UtilREST{
	
	public ClienteREST(){
		
	}
	
	@POST
	@Consumes("application/*")
	@Produces("application/json")
	public Response registrarCliente(String dados) {

		try {
			Cliente c = new ObjectMapper().readValue(dados, Cliente.class);		
			ClienteService clienteService=new ClienteService();
			clienteService.registrarCliente(c);

			return this.buildResponse("Registro efetuado com sucesso!!!!!");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	
	//Método utilizado para recuperar informações da tabela pela identificação recuperada
	@GET
	@Path("/{id}")
	@Consumes("application/*")
	public Response buscarClientesPorID(@PathParam("id")int id) {

		try {
			ClienteService clienteService=new ClienteService();
			Cliente c=clienteService.buscarClientesPorID(id);
			return this.buildResponse(c);
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@GET
	@Produces("application/*")
	public Response buscarClientes(){
		try{
			List<Cliente>listaClientes=new ArrayList<Cliente>();
			ClienteService clienteService=new ClienteService();
			listaClientes = clienteService.buscarClientes();
			return this.buildResponse(listaClientes);
		}catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}

		
		
	}
	
	@DELETE
	@Path("/{id}")
	public Response deletarCliente(@PathParam("id")int id){
		try{
			ClienteService clienteService=new ClienteService();
			clienteService.deletarCliente(id);
			return this.buildResponse("Registro excluido com sucesso.");
		}catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}

		
	}
	
	@PUT
	@Consumes("application/*")
	@Produces("application/json")
	public Response editarCliente(String dados) {

		try {
			Cliente c = new ObjectMapper().readValue(dados, Cliente.class);		
			ClienteService clienteService=new ClienteService();
			clienteService.editarCliente(c);

			return this.buildResponse("Registro editado com sucesso!!!!!");
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	

}
