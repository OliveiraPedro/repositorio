package br.com.pedro_campelo.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


//Nota��o que possibilita o mapeamento das classes que contem os recursos de persistencia ao banco de dados
//A nota��o @ApplicationPath permite com que classes REST possar ser mapeadas sem o arquivo web.xml.� uma alternativa para os desenvolvedores que
//usar gerenciadores de dependendias como MAVEN ou Ant, onde trabalham com referencias as bibliotecas e as baixam sempre que o projeto � iniciado
@ApplicationPath("rest")
public class Aplicacao extends Application{}
