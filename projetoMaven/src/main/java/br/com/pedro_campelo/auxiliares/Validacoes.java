package br.com.pedro_campelo.auxiliares;


public class Validacoes {
	public static  String validaCampo(String valorCampo, String nomeCampo) {

		String msg = "";

		if (valorCampo.trim().equals("")) {
			msg += "Preencha o campo " + nomeCampo + "." + "<br>";
		}

		return msg;

	}

	public String validaFone(String valorCampo, String nomeCampo) {

		String expFone = "\\d{10,11}";

		boolean valorFone = valorCampo.matches(expFone);

		String msg = "";
		if (!valorFone) {
			msg += "Preencha o campo " + nomeCampo + " corretamente.<br>";
		}
		return msg;
	}

	public String validaEmail(String valorCampo, String nomeCampo,String expressao) {
		String msg = "";

		boolean valorMail = valorCampo.matches(expressao);
		if (!valorMail) {
			msg += "Preencha o campo " + nomeCampo + " corretamente.<br>";
		}

		return msg;
	}

	public String validaSenha(String senha, String confSenha, String nomeCampo) {
		String msg = "";

		// String comparaSenha=senha.matches(confSenha);
		if (!confSenha.matches(senha)) {
			msg += nomeCampo;
		}
		return msg;
	}

	public String validaData(String verificaNascimento, String nomeCampo) {


		String expNasc="\\d{4}-\\d{2}-\\d{2}";


		String msg = "";

		boolean validaData=verificaNascimento.matches(expNasc);


		if(!validaData){
			msg += "Preencha o campo " + nomeCampo + " corretamente.<br>";
		}


		return msg;

	}

	public String validaHora(String verificaHorario, String nomeCampo) {


		String expHora="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$";


		String msg = "";

		boolean validaHorario=verificaHorario.matches(expHora);


		if(!validaHorario){
			msg += "Preencha o campo " + nomeCampo + " corretamente.<br>";
		}


		return msg;

	}

	public String validaNumeros(String numero, String nomeCampo,
			String expressao) {

		String msg = "";

		boolean teste = numero.matches(expressao);

		if (!teste) {
			msg += "Preencha o campo " + nomeCampo + " corretamente.<br>";
		}

		return msg;

	}

	//Método que valida a identificação enviada ao servidor(quando inteira), no caso de busca de informações, seja em um SELECT ou para UPDATE
	public static String validaID(int verificaFK) {

		String msg = "";

		if (verificaFK == 0) {
			msg += "Mande uma identificação correta.Falha na busca.<br>";
		}

		return msg;
	}

}
