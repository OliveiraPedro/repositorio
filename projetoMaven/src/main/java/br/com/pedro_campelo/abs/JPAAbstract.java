package br.com.pedro_campelo.abs;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.pedro_campelo.conexao.JPAConnection;
import br.com.pedro_campelo.dao.CrudDAO;

public abstract class JPAAbstract<T,ID> extends JPAConnection implements CrudDAO<T,ID>{
	// este m�todo ser� uma jun��o de prepareStatementGerandoId, e dos
	protected Class<T> entidade;

	@SuppressWarnings("unchecked")
	public JPAAbstract(){
		ParameterizedType superclass = (ParameterizedType)getClass().getGenericSuperclass();
		entidade=(Class<T>)superclass.getActualTypeArguments()[0];
	}

	public void incluir(T tipo) {
		// M�todo que efetua a persistencia com o banco de dados.Pode ser utilizado tanto com o envio de informa��es(CREATE,UPDATE)
		//tanto quanto a recupera��o de informa��es(SELECT) e a exclus�o de informa��es(DELETE)
		
		//A instancia em da classe EntityManager, recebe o m�todo getEntityManager(), herdado da classe JPAConnection, para que
		//possa efetuar o cadastro das informa��es com o m�todo persist() chamado apartir do mesmo.
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.persist(tipo);
		em.getTransaction().commit();
		em.close();
	}
	//Este m�todo retorna outro m�todo, getEntityManager(),que encadeia o m�todo find(), recebendo o class da entidade,
	// al�m de uma chave para rtazer informa��es especificas
	public T buscarPorId(ID id){
		return getEntityManager().find(entidade, id);
	}

	public T atualizar(T t){
		//A instancia em da classe EntityManager, recebe o m�todo getEntityManager(), herdado da classe JPAConnection, para que
		//possa atualizar o banco de dados com o m�todo merge() chamado a partir do mesmo.
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.merge(t);
		em.getTransaction().commit();
		em.close();
		return t;
	};
	public void deletar(ID id){
		//A instancia em da classe EntityManager, recebe o m�todo getEntityManager(), herdado da classe JPAConnection, para que
		//possa atualizar o banco de dados com o m�todo merge() chamado a partir do mesmo.
		EntityManager em = getEntityManager();
		T t = em.find(entidade, id);
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	};
	public List<T> buscarTodos(){
		//Para efetuar a consulta ao banco de dados � feita a utiliza��o da classe TypedQuery, que pode receber tipos gen�ricos,
		// o que facilita na hora de determinar o tipo do retorno que � recebido.
		
		//Neste caso, o TypedQuery<T> recebe a entidade(objeto/pojo) que � passada pela interface(interface JPADAO)que estends a
		//JPAAbstract
		
		/*Depois do JPAAbstract receber a entidade, o seu construtor faz um espelhamento para que possa inicializar uma variavel com o 
		 seu .class, usado na consulta a seguir, sendo que para isso � feita a utiliza��o do m�todo createQuery(), da classe EntityManager
		recebendo a entidade e recuperando o seu nome, que dependendo das anota��es utilizadas, permanece o mesmo.
		No final uma lista (List<T>) recebendo um tipo gen�rico � montado com o retorno da cunsulta feita.*/ 
		
		TypedQuery<T> consulta=super.getEntityManager().createQuery("SELECT q from "+entidade.getSimpleName()+" q",entidade);
		List<T> retorno=consulta.getResultList();
		return retorno;
	};
}
