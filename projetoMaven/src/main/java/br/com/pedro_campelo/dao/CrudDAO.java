package br.com.pedro_campelo.dao;

import java.util.List;


public interface CrudDAO<T,ID> {
	public void incluir(T t);
	public T buscarPorId(ID id);
	public T atualizar(T t);
	public void deletar(ID id);
	public List<T> buscarTodos();
}
