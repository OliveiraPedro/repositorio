package br.com.pedro_campelo.dao;

import br.com.pedro_campelo.objetos.Cliente;

public interface ClienteDAO extends CrudDAO<Cliente,Integer>{

}
