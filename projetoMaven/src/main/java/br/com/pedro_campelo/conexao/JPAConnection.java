package br.com.pedro_campelo.conexao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class JPAConnection {
	private static EntityManagerFactory conexao;

	private EntityManager em;
	
	//M�todo do tipo EntityManagerFactory que cria a conex�o ao banco de dados no momento em que � feita a persistencia
	//de dados
	private EntityManagerFactory conectar() {
		try {
			if (conexao != null && conexao.isOpen()) {
				return conexao;
			}
		} catch (Exception e) {
		}
		
		//O parametro de conex�o recebe a unidade de persistencia do projeto, recebendo todas as informa��es
		//que foram descritas no persistence.xml
		conexao = Persistence.createEntityManagerFactory("CLIENTE");
		return conexao;
		// EntityManager em = conexao.createEntityManager();
	}

	// M�todo que efetua a persistencia com o banco de dados.Pode ser utilizado tanto com o envio de informa��es(CREATE,UPDATE)
	//tanto quanto a recupera��o de informa��es(SELECT) e a exclus�o de informa��es(DELETE)
	public EntityManager getEntityManager() {
		
		if(em==null || !em.isOpen()){
			em=conectar().createEntityManager();
		}
		
		
		return em;
	}

	// M�todo que auxilia na busca de resultados no banco de dados.Faz a parte do SELECT dentro de um CRUD
	public Query getQuery(String jpql) {
		return this.getEntityManager().createQuery(jpql);
	}


}