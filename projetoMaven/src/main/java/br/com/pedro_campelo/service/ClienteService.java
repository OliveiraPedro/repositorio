package br.com.pedro_campelo.service;

import java.util.List;

import br.com.pedro_campelo.FACTORY.DAOFactory;
import br.com.pedro_campelo.auxiliares.Validacoes;
import br.com.pedro_campelo.dao.ClienteDAO;
import br.com.pedro_campelo.exceptions.MainException;
import br.com.pedro_campelo.objetos.Cliente;

public class ClienteService {
	
	
	public void registrarCliente(Cliente c) throws MainException {
		
		
		//M�todo que valida as informa��es enviadas pelo usu�rio
		validaInfo(c);
		
		//� passado para o objeto persist a instaancia do m�todo static getInstanceOf(), que retorna uma interface que cont�m acesso 
		//aos m�todos de persistencia
		ClienteDAO persist= (ClienteDAO) DAOFactory.getInstanceOf(ClienteDAO.class);
	
		persist.incluir(c);
	}

	public Cliente buscarClientesPorID(int id) throws MainException {
		//M�todo que valida a identifica��o enviada pelo usu�rio
		validaInfoID(id);
		
		//� passado para o objeto persist a instaancia do m�todo static getInstanceOf(), que retorna uma interface que cont�m acesso 
		//aos m�todos de persistencia
		ClienteDAO persist= (ClienteDAO) DAOFactory.getInstanceOf(ClienteDAO.class);
		return persist.buscarPorId(id);			
	}

	public List<Cliente> buscarClientes() {
		ClienteDAO persist= (ClienteDAO) DAOFactory.getInstanceOf(ClienteDAO.class);

		System.out.println(persist);
		return persist.buscarTodos();
	}

	public void deletarCliente(int id) {
		ClienteDAO persist= (ClienteDAO) DAOFactory.getInstanceOf(ClienteDAO.class);
		persist.deletar(id);
	}

	public void editarCliente(Cliente c) throws MainException {
		//M�todo que valida a identifica��o enviada pelo usu�rio
		validaInfo(c);
		
		ClienteDAO persist= (ClienteDAO) DAOFactory.getInstanceOf(ClienteDAO.class);
		persist.atualizar(c);
	}
	
	private void validaInfo(Cliente c) throws MainException {

		String mensagem=Validacoes.validaCampo(c.getNome(), " NOME");
		
		if(!mensagem.equals("")){
			throw new MainException(mensagem);
		}
	}
	
	private void validaInfoID(int id) throws MainException {

		String mensagem=Validacoes.validaID(id);
		
		if(!mensagem.equals("")){
			throw new MainException(mensagem);
		}
	}
}
