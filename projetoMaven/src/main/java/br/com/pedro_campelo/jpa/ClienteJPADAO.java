package br.com.pedro_campelo.jpa;

import br.com.pedro_campelo.abs.JPAAbstract;
import br.com.pedro_campelo.dao.ClienteDAO;
import br.com.pedro_campelo.objetos.Cliente;

public class ClienteJPADAO extends JPAAbstract<Cliente,Integer> implements ClienteDAO{
}
